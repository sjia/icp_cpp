#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <string>

#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkIterativeClosestPointTransform.h"
#include "vtkLandmarkTransform.h"
#include "vtkPolyData.h"
#include "vtkMatrix4x4.h"
#include "vtkSmartPointer.h"
#include "vtkTransformPolyDataFilter.h"

/**
 * Main function.
 */



int main( int argc, char * argv[] )
{

    if( argc >5 )
      {
        std::cerr << "Usage: "
                  << std::endl
                  << argv[0]
                  << "movingMesh fixedMesh outputMesh [degree of liberty, (default) 0 rigid, 1 similarity, 2 affine ]"
                  << std::endl;

        return EXIT_FAILURE;
      }


    std::string name_movingMesh=argv[1];
    std::string name_fixedMesh=argv[2];
    std::string name_outputMesh=argv[3];

    int degreeLib=0;
    if (argc==5)
        degreeLib=std::stoi(argv[4]);


    std::cout << "ICP matching :" << std::endl;
    std::cout << "Moving : " << name_movingMesh<< std::endl;
    std::cout << "Fixed : " << name_fixedMesh << std::endl;
    std::cout << "Outpout : " <<name_outputMesh << std::endl;


    vtkPolyDataReader * readerF = vtkPolyDataReader::New();
    readerF->SetFileName(name_fixedMesh.c_str());
    readerF->Update();

    vtkPolyData * fixedMesh = readerF->GetOutput();


    vtkPolyDataReader * readerM = vtkPolyDataReader::New();
    readerM->SetFileName(name_movingMesh.c_str());
    readerM->Update();

    vtkPolyData * movingMesh = readerM->GetOutput();

    vtkIterativeClosestPointTransform * transformICP =vtkIterativeClosestPointTransform::New();

    transformICP->SetSource(movingMesh);
    transformICP->SetTarget(fixedMesh);

    if (degreeLib==0)
    {
        std::cout << "Rigid Transform" << std::endl;

        transformICP->GetLandmarkTransform()->SetModeToRigidBody();
    }
    if (degreeLib==1)
    {
        std::cout << "Similarity Transform" << std::endl;

        transformICP->GetLandmarkTransform()->SetModeToSimilarity();
    }
    if (degreeLib==2)
    {
        std::cout << "Affine Transform" << std::endl;

        transformICP->GetLandmarkTransform()->SetModeToAffine();
    }
    double ScaleFactor=1;
    int MaxNumIterations=50;
    int MaxNumLandmarks=200;
    double MaxMeanDistance=0.01;

    transformICP->StartByMatchingCentroidsOn();
    transformICP->SetMaximumNumberOfLandmarks(MaxNumLandmarks);
    transformICP->SetMaximumNumberOfIterations(MaxNumIterations);
    transformICP->SetMaximumMeanDistance(MaxMeanDistance);
 
    transformICP->Modified();
    transformICP->Update();

    vtkSmartPointer<vtkMatrix4x4> m = transformICP->GetMatrix();
     std::cout << "The resulting matrix is: " << *m << std::endl;


     vtkSmartPointer<vtkTransformPolyDataFilter> icpTransformFilter =
        vtkSmartPointer<vtkTransformPolyDataFilter>::New();
      icpTransformFilter->SetInputData(movingMesh);
      icpTransformFilter->SetTransform(transformICP);
      icpTransformFilter->Update();

    vtkPolyDataWriter * writer = vtkPolyDataWriter::New();
    writer->SetInputData(icpTransformFilter->GetOutput());
    writer->SetFileName(name_outputMesh.c_str());
    writer->Update();


    return EXIT_SUCCESS;

}
